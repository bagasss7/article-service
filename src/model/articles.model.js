const Joi = require('joi');

const articlesModel = Joi.object({
  author_name: Joi.string().required(),
  title: Joi.string().required(),
  body: Joi.string().required(),
});

module.exports = articlesModel;