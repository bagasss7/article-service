const Joi = require('joi');

const authorsModel = Joi.object({
  name: Joi.string().required()
})

module.exports = authorsModel;