const {MongoClient} = require('mongodb');
// Connection URI
const uri = "mongodb+srv://bagasss7:bagas123@cluster0.nr1s7.mongodb.net/Kumparan?retryWrites=true&w=majority"

// Create MongoClient
const client = new MongoClient(uri, { useUnifiedTopology: true, useNewUrlParser: true  });

const connect = async() =>{
  try{
    await client.connect();
    console.log('Connected to Database!');
  }
  catch(err){
    await client.close();
  }
  
}

const close = async() =>{
  try{
    await client.close();
  }
  catch(err){
    console.log(err);
  }
  
}

const clear = async() =>{
  try{
    await client.clear();
  }
  catch(err){
    console.log(err);
  }
}

const getDB = () =>{
  let db = client.db('Kumparan');
  return db;
}
module.exports= {connect,getDB,close,clear}