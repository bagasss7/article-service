const db = require('../database/db');
const valCreateArticle = require('../model/articles.model');
/**
 * 
 * @function createArticles
 * @description author create the articles
 */
exports.createArticles = async(req,res)=>{
  try{
    const {author_name,title,body} = req.body;

    // validate create Article
    const val = valCreateArticle.validate(req.body);

    // if validate error
    if(val.error){
      return res.status(400).send({
        success: false,
        message: val.error.details[0].message
      });
    }

    // check author exist or not
    const author = await db.getDB().collection('authors').findOne({name:author_name});
    
    // if author doesn't exist
    if(!author){
      return res.status(404).json({
        success: false,
        message: `Author ${author_name} doesn't exist!`
      });
    }

    // object to save into database
    const unserInput = {
      author_id: author._id,
      title,
      body,
      created_at: new Date()
    }
    
    // save to collection articles
    const result = await db.getDB().collection('articles').insertOne(unserInput);

    if(!result){
      return res.status(400).json({
        success: false,
        message: 'Error when create new article'
      });

    }

    return res.status(201).json({
      success: true,
      message: result
    });
  }
  catch(err){
    return res.status(400).json({
      success: false,
      message: 'Error occured'
    });
  }
}

/**
 * 
 * @function searchArticles
 * @description search articles by query / author / all articles
 */
exports.searchArticles = async(req,res) =>{
  try{
    const query = req.query.query;
    const author = req.query.author;
    let result=null;

    // if search by query
    if(query){

      // find keyword in body OR title & sort by latest
      result = await db.getDB().collection('articles').find({
        $or:[
          {
            body: {
              $regex: new RegExp(".*" + query + ".*"),
              $options: "i",
            }
          },
          {
            title: {
            $regex: new RegExp(".*" + query + ".*"),
            $options: "i",
            }
          }
    
        ]
      }).sort([["_id", -1]]).toArray();
      if(result.length<1){
        return res.status(404).json({
          success: false,
          message: `No articles match with this keyword!`
        });
      }
    }

    // if search by author query
    else if(author){

      // check if author exist
      const authorSearch = await db.getDB().collection('authors').findOne({name:author});
      
      if(!authorSearch){
        return res.status(404).json({
          success: false,
          message: `Author ${author} doesn't exist!`
        });
      }
      result = await db.getDB().collection('articles').find({author_id:authorSearch._id}).sort([["_id", -1]]).toArray();
    }

    // search all articles
    else{
      result = await db.getDB().collection('articles').find().sort([["_id", -1]]).toArray();
      
    }
    return res.status(200).json({
      success: true,
      data: result
    })
  }
  catch(err){
    return res.status(400).json({
      success: false,
      message: 'Error occured'
    });
  }
}