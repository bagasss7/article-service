const db = require('../database/db');
const valCreateAuthor = require('../model/authors.model');

/**
 * 
 * @function getAllAuthors
 * @description get all authors
 */
exports.getAllAuthors = async(req,res) =>{
  try{

    // find all authors 
    const result = await db.getDB().collection('authors').find({}).toArray();
    if(!result){
      return res.status(400).json({
        success: false,
        message: 'Error'
      });
    }

    return res.status(200).json({
      success: true,
      message: result
    });
  }
  catch(err){
    return res.status(400).json({
      success: false,
      message: 'Error occured'
    });
  }
}

/**
 * 
 * @function createAuthors
 * @description create author to create articles
 */
exports.createAuthors = async(req,res)=>{
  try{
    const unserInput = req.body;

    // validate author data input
    const val = valCreateAuthor.validate(req.body);
    if(val.error){
      return res.status(400).send({
        success: false,
        message: val.error.details[0].message
      });
    }

    // save into collection authors 
    const result = await db.getDB().collection('authors').insertOne(unserInput);

    if(!result){
      return res.status(400).json({
        success: false,
        message: 'Error'
      });

    }

    return res.status(201).json({
      success: true,
      message: result
    });
  }
  catch(err){
    return res.status(400).json({
      success: false,
      message: 'Error occured'
    });
  }
  
}