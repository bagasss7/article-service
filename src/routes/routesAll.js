const articles = require('./articles.routes');
const authors = require('./authors.routes');

const routes = [
  {
    path:'/articles',
    api: articles
  },
  {
    path:'/authors',
    api: authors
  }
]

module.exports = routes;