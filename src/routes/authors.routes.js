const express = require('express');
const routes = express.Router();

const authorsController = require('../controller/authors.controller');

/**
 * @route /authors
 */
routes.get('/',authorsController.getAllAuthors);
routes.post('/',authorsController.createAuthors);

module.exports=routes;