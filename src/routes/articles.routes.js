const express = require('express');
const routes = express.Router();
const articlesController = require('../controller/articles.controller');

/**
 * @route /articles
 */

routes.post('/',articlesController.createArticles);
routes.get('/',articlesController.searchArticles);

module.exports=routes;