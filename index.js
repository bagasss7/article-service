const app = require('./app');
const PORT = 3000;
const server = require('./src/database/db');

app.listen(PORT,()=>{
  server.connect();
  console.log(`Listening on PORT:${PORT}`)
})