const app = require('../app');
const request = require('supertest');
const server = require('../src/database/db');

beforeAll(async()=>{
  await server.connect()
});

afterAll(async()=>{
  //drop collection in order to avoid false positive
  await server.getDB().collection('articles').drop();
  await server.getDB().collection('authors').drop();
  await server.close();
});

describe('Post a new article', ()=>{

  const createSuccess = {
    message: {
      "acknowledged": true,
      "insertedId": expect.any(String),
    },
    success: true,
  }

  describe('Create author', ()=>{

    const authorData = {
      name: "Bagas"
    }

    it('should return 201 status and success as true', async()=>{
      const res = await request(app).post('/authors')
      .send(authorData)
      .set('Accept','application/json')
      expect(res.status).toBe(201);
      expect(res.body).toEqual(createSuccess);
    })
  })

    describe('POST /articles if the author exist', ()=>{
      const articleDataWithValidAuthor = {
        author_name: "Bagas",
        title: "Walikota bekasi ditangkap",
        body: "Walikota bekasi ditangkap karena kasus korupsi akhirnya ditangkap"
      }

      it('should return 201 status and success as true', async()=>{
        const res = await request(app).post('/articles')
        .send(articleDataWithValidAuthor)
        .set('Accept','application/json')
        expect(res.status).toBe(201);
        expect(res.body).toEqual(createSuccess);
      })
    })
    
    describe('POST /articles if the author doesn\'t exist', ()=>{

      const articleDataWithNoValidAuthor = {
        author_name: "Author's not exist",
        title: "Test Title",
        body: "Test Body"
      }

      const createNotSuccess = {
        message: expect.any(String),
        success: false,
      }

      it('should return 404 status and success as false', async()=>{
        const res = await request(app).post('/articles')
        .send(articleDataWithNoValidAuthor)
        .set('Accept','application/json')
        expect(res.status).toBe(404);
        expect(res.body).toEqual(createNotSuccess);
      })
    })
})


describe('GET a list of articles', ()=>{
    describe('GET all articles', ()=>{
      it('should return 200 status', async()=>{
        const res = await request(app).get('/articles');
        expect(res.status).toEqual(200);
        
      })
    })
    
    describe('GET articles filter by keyword in title and body', ()=>{
      it('should return 200 status', async()=>{
        const res = await request(app).get('/articles?query=bekasi');
        expect(res.status).toEqual(200);
        
      })
    })
    
    describe('GET articles filter by keyword in title and body if the keyword doesn\'t exist', ()=>{
      it('should return 404 status', async()=>{
        const res = await request(app).get('/articles?query=shouldnotexist');
        expect(res.status).toEqual(404);
        
      })
    })

    describe('GET articles filter by author', ()=>{
      it('should return 200 status', async()=>{
        const res = await request(app).get('/articles?author=Bagas');
        expect(res.status).toEqual(200);
        
      })
    })

    describe('GET articles filter by author if the author doesn\'t exist', ()=>{
      it('should return 404 status', async()=>{
        const res = await request(app).get('/articles?author=baswara');
        expect(res.status).toEqual(404);
        
      })
    })
})



