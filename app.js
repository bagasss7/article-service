const express = require('express');
const app = express();
const routes = require('./src/routes/routesAll');



app.use(express.json());
routes.forEach((route) => {
  app.use(route.path,route.api);
});

module.exports=app;